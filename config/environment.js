/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'opendata-hackaton',
    environment: environment,
    baseURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      }
    },

    APP: {
        SERVER_HOST: null,
        BACKEND_HOST: 'http://localhost:4200'
      // Here you can pass flags/options to your application instance
      // when it is created
    },

      contentSecurityPolicy: {
          'default-src': "'none'",
          'script-src': "'self' 'unsafe-eval' 'unsafe-inline' https://www.google-analytics.com http://www.google.com https://www.google.com http://ajax.googleapis.com",
          'font-src': "'self' https://fonts.gstatic.com",
          'style-src': "'self' 'unsafe-inline' https://fonts.googleapis.com http://www.google.com http://ajax.googleapis.com https://ajax.googleapis.com ",
          'connect-src': "'self' http://opendata-hackathon.com:3000 http://api-srv.opendata-hackathon.com http://opendata-backend.strokit.net",
          'img-src': "'self' http://lorempixel.com data:",
          'media-src': "'self'"
      }
  };

  if (environment === 'development') {
      //ENV.APP.SERVER_HOST = "http://api-srv.opendata-hackathon.com";
      //ENV.APP.BACKEND_HOST = "http://opendata-backend.strokit.net";
      // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.baseURL = '/';
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {
      ENV.APP.SERVER_HOST = "http://api-srv.opendata-hackathon.com";
      ENV.APP.BACKEND_HOST = "http://opendata-backend.strokit.net";
  }

  return ENV;
};
