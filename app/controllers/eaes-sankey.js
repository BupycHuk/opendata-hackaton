import Ember from 'ember';
import config from '../config/environment';

export default Ember.Controller.extend({

    refreshChart: function () {
        var that = this;
        var years = this.get('selectedYears').map(function(item) {
            return item.get('value')
        });
        var params = {
            'countries': this.get('selectedCountries'),
            'products': this.get('selectedProducts'),
            'years': years
        };
        $.getJSON(config.APP.BACKEND_HOST + "/eaes-sankey/" + this.get('type') + "/", params)
            .then(function (response) {
                var model = response.map(function (item) {
                    return Ember.Object.create(item);
                });
                that.set('model', model);
            });
    }.observes('selectedCountries', 'selectedProducts', 'selectedYears.length', 'countries', 'products'),

    init: function () {
        this._super();
        var that = this;
        $.getJSON(config.APP.BACKEND_HOST + "/eaes-countries").then(function (response) {
            that.set('countries', response.map(function(data) {
                return Ember.Object.create(data);
            }));
        });
        $.getJSON(config.APP.BACKEND_HOST + "/eaes-products").then(function (response) {
            that.set('products', response.map(function(data) {
                return Ember.Object.create(data);
            }));
        });
    },
    years: function(){
        var years = [];
        for (var i = 2001; i < 2015; i++) {
            years.push(Ember.Object.create({'value': i}));
        }
        return years;
    }.property(),
    countries: [],
    products: [],
    selectedCountries: [],
    selectedProducts: [14],
    selectedYears: [],
    type: 'import'
});
