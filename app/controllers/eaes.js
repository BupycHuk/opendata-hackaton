import Ember from 'ember';
import config from '../config/environment';

export default Ember.Controller.extend({

    refreshChart: function () {
        var that =this;
        $.getJSON(config.APP.BACKEND_HOST + "/eaes-countries/" + this.get('type') + "/" + this.get('selectedCountry') + "/" , { 'products': this.get('selectedProducts')}).then(function (response) {
            var model = response.map(function(item) {
                return Ember.Object.create(item);
            });
            that.set('model', model);
        });
    }.observes('selectedCountry', 'type', 'selectedProducts'),
    init: function() {
        this._super();
        var that = this;
        $.getJSON(config.APP.BACKEND_HOST + "/eaes-countries").then(function (response) {
            that.set('countries', response);
        });
        $.getJSON(config.APP.BACKEND_HOST + "/eaes-products").then(function (response) {
            that.set('products', response);
        });
        this.refreshChart();
    },
    type: 'import',
    countries: null,
    products: null,
    selectedCountry: 4,
    selectedProducts: null
});
