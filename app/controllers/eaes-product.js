import Ember from 'ember';
import config from '../config/environment';

export default Ember.Controller.extend({

    refreshChart: function () {
        var that =this;
        $.getJSON(config.APP.BACKEND_HOST + "/eaes-products/" + this.get('type') + "/" + this.get('selectedProduct') + "/" , { 'countries': this.get('selectedCountries')}).then(function (response) {
            var model = response.map(function(item) {
                return Ember.Object.create(item);
            });
            that.set('model', model);
        });
    }.observes('selectedProduct', 'type', 'selectedCountries'),
    init: function() {
        this._super();
        var that = this;
        $.getJSON(config.APP.BACKEND_HOST + "/eaes-countries").then(function (response) {
            that.set('countries', response);
        });
        $.getJSON(config.APP.BACKEND_HOST + "/eaes-products").then(function (response) {
            that.set('products', response);
        });
        this.refreshChart();
    },
    type: 'import',
    countries: null,
    products: null,
    selectedProduct: 14,
    selectedCountries: null
});
