import Ember from 'ember';

export default Ember.View.extend({
    myLineChart: null,

    didInsertElement: function() {
        this.renderChart();
    },
    renderChart: function() {
        var cleanedData = this.get('controller.model');
        if (cleanedData == null || cleanedData.length == 0) {
            return;
        }
        var data = {
            labels: [],
            datasets: []
        };
        for (var i = 2001; i< 2015; i++)
        {
            data.labels.push(i);
        }

        cleanedData.forEach(function(item) {

            var selectedCountry = item;

            var countryLine = {
                label: selectedCountry.get('label'),
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(50,50,50,1)",
                pointColor: "#0071A4",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: selectedCountry.get('values')
            };
            data.datasets.push(countryLine);
        });
        var myLineChart = this.get('myLineChart');
        if (myLineChart != null) {
            myLineChart.stop();
            myLineChart.destroy();
        }
        var ctx = $("#eaes-info").get(0).getContext("2d");
        this.set('myLineChart', new Chart(ctx).Line(data, {

            tooltipTemplate: "<%=label%>: <%= value %>",
            multiTooltipTemplate: "<%= datasetLabel %>: <%= value %>",
            scaleLabel: "$<%=value%>000"
        }));
    }.observes('controller.model')

});
