import Ember from 'ember';

export default Ember.View.extend({
    showTaxChart: function () {
        var data = {
            labels: ["Страны ТС", "Беларусь-Казахстан", "Беларусь-Россия", "Казахстан-ТС"],
            datasets: [
                {
                    label: "2009г",
                    fillColor: "rgba(220,220,220,0.5)",
                    strokeColor: "rgba(220,220,220,0.8)",
                    highlightFill: "rgba(220,220,220,0.75)",
                    highlightStroke: "rgba(220,220,220,1)",
                    data: [36000, 464.8, 9900, 12900]
                },
                {
                    label: "2013г",
                    fillColor: "rgba(0,0,220,0.5)",
                    strokeColor: "rgba(220,220,220,0.8)",
                    highlightFill: "rgba(0,0,220,0.75)",
                    highlightStroke: "rgba(0,0,220,1)",
                    data: [65000, 870.4, 16900, 24600]
                }
            ]
        };
        var ctx = $("#trade-eaes").get(0).getContext("2d");
        new Chart(ctx).Bar(data, {

            tooltipTemplate: "<%=label%>: <%= value %>",
            multiTooltipTemplate: "<%= datasetLabel %>: <%= value %>",
            scaleLabel: "$<%=value%> млн."
        });
    },
    showTradeChart: function () {
        var data = {
            labels: ["Кыргызстан", "Армения", "Беларусь", "Казахстан", "Россия"],
            datasets: [
                {
                    label: "Налоги стран ЕАЭС",
                    fillColor: "rgba(0,0,220,0.5)",
                    strokeColor: "rgba(220,220,220,0.8)",
                    highlightFill: "rgba(0,0,220,0.75)",
                    highlightStroke: "rgba(0,0,220,1)",
                    data: [10, 30, 20, 20, 20]
                }
            ]
        };
        var ctx = $("#tax-eaes").get(0).getContext("2d");
        new Chart(ctx).Bar(data,{

            tooltipTemplate: "<%=label%>: <%= value %>",
            multiTooltipTemplate: "<%= datasetLabel %>: <%= value %>",
            scaleLabel: "<%=value%>%"
        });
    },
    didInsertElement: function() {
        this.showTaxChart();
        this.showTradeChart();
    }
});
