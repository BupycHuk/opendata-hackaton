import Ember from 'ember';

export default Ember.View.extend({
    sankey: null,
    didInsertElement: function () {

        var countries = this.get('controller.countries');
        var products = this.get('controller.products');
        var years = this.get('controller.years').map(function(item) {
            return item.get('value');
        });
        var model = this.get('controller.model');
        if (!products || !countries || !model) {
            return
        }

        var data = new google.visualization.DataTable();

        data.addColumn('string', 'From');
        data.addColumn('string', 'To');
        data.addColumn('number', 'Сумма в тысячах долларов США');

        model.forEach(function(item) {
            if (item.usd != 0) {
                data.addRows([[
                    item.get('year')+'',
                    item.get('country'),
                    item.get('usd')
                ],[
                    item.get('country'),
                    item.get('product'),
                    item.get('usd')
                ]]);
            }
        });
        products = products.map(function (item) {
            return item.title
        });
        countries = countries.map(function (item) {
            return item.title
        });

        var sankey = this.get('sankey');
        if (!sankey)
            sankey = new google.visualization.Sankey(document.getElementById('sankey'));

        // Instantiate and draw our chart, passing in some options.
        sankey.draw(data, {});

        this.set('sankey', sankey);
    }.observes('controller.model')
});
