import Ember from 'ember';

export default Ember.Route.extend({
    model: function() {
        this.store.find('country');
        this.store.find('goods');
    },
    actions: {
        toggleSidebar: function() {
            this.get('controller').toggleProperty('sidebarVisible');
        }
    }
});
