import Ember from 'ember';

export default Ember.Component.extend({
    title: null,
    actions: {
        MenuClick: function() {
            this.sendAction();
        }
    }
});
