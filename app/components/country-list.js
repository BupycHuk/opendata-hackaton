import Ember from 'ember';
import $ from 'jquery';

export default Ember.Component.extend({
    myLineChart: null,
    myDoughnutChart: null,
    selectedCountryCode: null,
    selectedCountryName: function() {
        if (this.get('selectedCountryCode') != null) {

            var cleanedData = this.get('cleanedData');
            var code = this.get('selectedCountryCode');

            var selectedCountry = cleanedData[code];
            return selectedCountry[0].get('country');
        }
    }.property('selectedCountryCode'),
    doughnutChartColors: {
        colors: {
            2011: "#F7464A",
            2012: "#46BFBD",
            2013: "#FDB45C",
            2014: "#78FB11"
        },
        highlights: {
            2011: "#FF5A5E",
            2012: "#5AD3D1",
            2013: "#FFC870",
            2014: "#87DD23"
        }
    },

    showDoughnutChart: function (selectedCountry) {
        var that = this;
        var data = [
        ];
        selectedCountry.forEach(function (item) {
            data.push(
                {
                    value: item.get('usd_sum'),
                    color: that.doughnutChartColors.colors[item.get('year')],
                    highlight: that.doughnutChartColors.highlights[item.get('year')],
                    label: item.get('year')
                }
            );
        });
        var myDoughnutChart = this.get('myDoughnutChart');
        if (myDoughnutChart != null) {
            myDoughnutChart.destroy();
        }
        var ctx = $("#country-detail-pie").get(0).getContext("2d");
        myDoughnutChart = new Chart(ctx).Doughnut(data);
        this.set('myDoughnutChart', myDoughnutChart);
    },
    showLineChart: function (selectedCountry) {
        var data = {
            labels: [],
            datasets: [
                {
                    label: selectedCountry.get('country'),
                    fillColor: "rgba(220,220,220,0.2)",
                    strokeColor: "rgba(50,50,50,1)",
                    pointColor: "#0071A4",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: []
                }
            ]
        };
        selectedCountry.forEach(function (item) {
            data.labels.push(item.get('year'));
            data.datasets[0].data.push(item.get('usd_sum'));
        });
        var myLineChart = this.get('myLineChart');
        if (myLineChart != null) {
            if (myLineChart.datasets[0].points != null) {
                while (myLineChart.datasets[0].points.length > 0) {
                    myLineChart.removeData();
                }
            }
            selectedCountry.forEach(function (item) {
                myLineChart.addData(
                    [item.get('usd_sum')],
                    item.get('year')
                );
            });
        } else {
            var ctx = $("#country-detail-line").get(0).getContext("2d");
            this.set('myLineChart', new Chart(ctx).Line(data, {
                scaleLabel: "$<%=value%>000",
                multiTooltipTemplate: "<%= datasetLabel %>: <%= value %>"
            }));
        }
    },

    didChangeSelectedCountry: function() {
        var cleanedData = this.get('cleanedData');
        var code = this.get('selectedCountryCode');
        var selectedCountry = cleanedData[code];
        if (selectedCountry == null) {
            $('#tabs').hide();
            return;
        }
        $('#tabs').show();
        this.showLineChart(selectedCountry);
        this.showDoughnutChart(selectedCountry);

    }.observes('selectedCountryCode'),

    didInsertElement: function () {
        var gdpData = this.get('data');
        var that = this;
        $(function () {
            $('#world-map-gdp').vectorMap({
                map: 'world_mill_en',
                backgroundColor: '#FFFFFF',
                regionStyle: {
                    initial: {
                        fill: '#BBBBBB',
                        "fill-opacity": 1,
                        stroke: 'none',
                        "stroke-width": 0,
                        "stroke-opacity": 1
                    },
                    hover: {
                        "fill-opacity": 0.8,
                        cursor: 'pointer'
                    },
                    selected: {
                        fill: 'green'
                    },
                    selectedHover: {
                    }
                },
                series: {
                    regions: [{
                        values: gdpData,
                        scale: ['#C8EEFF', '#0071A4'],
                        normalizeFunction: 'polynomial'
                    }]
                },
                regionsSelectableOne: true,
                regionsSelectable: true,
                focusOn: {
                    lat: 45,
                    lng: 70,
                    scale: 3
                },
                onRegionSelected: function(e, code) {
                    that.set('selectedCountryCode', code);
                },
                onRegionTipShow: function (e, el, code) {
                    if (gdpData[code] == null) {
                        return;
                    }
                    var text = $('<div/>');
                    var usd = $('<span/>').html("Сумма в долларах: " + gdpData[code]*1000);
                    text.append(usd);
                    text.appendTo(el);
                    el.html(el.html());
                }
            });
        });
        $('#tabs').hide();
    },

    cleanedData: function () {
        var result = [];
        this.get('model').forEach(function(item){
            if (result[item.get('code.code')] == null) {
                result[item.get('code.code')] = [];
            }
            result[item.get('code').get('code')].push(Ember.Object.create({
                country: item.get('code.title'),
                year: item.get('year'),
                usd_sum: item.get('usd_sum'),
                som_sum: item.get('som_sum')
            }));
        });
        return result;
    }.property('model'),

    data: function () {
        var result = {};
        this.get('model').forEach(function(item){
            if (result[item.get('code').get('code')] == null) {
                result[item.get('code').get('code')] = item.get('usd_sum');
            } else {
                result[item.get('code').get('code')] += item.get('usd_sum');
            }
        });
        return result;
    }.property('model')
});
