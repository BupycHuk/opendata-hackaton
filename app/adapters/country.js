import ApplicationAdapter from "./application";

export default ApplicationAdapter.extend({
    pathForType: function() {
        return 'foreign_trade_country';
    }
});
