import DS from 'ember-data';
import config from '../config/environment';

export default DS.RESTAdapter.extend({
    host: config.APP.BACKEND_HOST,
    defaultSerializer: '-default'
});