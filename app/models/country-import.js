import TradeBase from './trade-base';
import DS from 'ember-data';

export default TradeBase.extend({
    code: DS.belongsTo('country')
});
