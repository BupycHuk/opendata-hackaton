import DS from 'ember-data';

export default DS.Model.extend({
    year: DS.attr('number'),
    weight: DS.attr(),
    usd_sum: DS.attr(),
    som_sum: DS.attr()
});