module.exports = function (app) {
    var express = require('express');
    var postRouter = express.Router();

    postRouter.get('/foreign_trade_by_country_import', function (req, res) {
        res.send(
            [
                {
                    id: 1,
                    code: 1,
                    year: 2011,
                    weight: 11585.3,
                    usd_sum: 11153,
                    som_sum: 510431
                },
                {
                    id: 2,
                    code: 2,
                    year: 2011,
                    weight: 79.4,
                    usd_sum: 405.1,
                    som_sum: 18585.8
                },
                {
                    id: 3,
                    code: 3,
                    year: 2011,
                    weight: 77822.6,
                    usd_sum: 109254,
                    som_sum: 5017780
                },
                {
                    id: 4,
                    code: 4,
                    year: 2011,
                    weight: 2686660,
                    usd_sum: 385492,
                    som_sum: 17761300
                },
                {
                    "id": 97,
                    "code": 1,
                    "year": 2012,
                    "weight": 56.1,
                    "usd_sum": 794.1,
                    "som_sum": 37317.8
                },
                {
                    "id": 98,
                    "code": 2,
                    "year": 2012,
                    "weight": 73680.9,
                    "usd_sum": 71901.7,
                    "som_sum": 3381210
                },
                {
                    "id": 99,
                    "code": 3,
                    "year": 2012,
                    "weight": 6648.2,
                    "usd_sum": 9750.5,
                    "som_sum": 458727
                },
                {
                    "id": 100,
                    "code": 4,
                    "year": 2012,
                    "weight": 12803.9,
                    "usd_sum": 18717.6,
                    "som_sum": 879975
                }
            ]
        );
    });

    postRouter.get('/foreign_trade_goods', function (req, res) {
        res.send(
            [
                {
                    "id": 1,
                    "code": 1,
                    "title": "ЖИВЫЕ ЖИВОТНЫЕ"
                },
                {
                    "id": 2,
                    "code": 2,
                    "title": "МЯСО И ПИЩЕВЫЕ МЯСНЫЕ СУБПРОДУКТЫ"
                },
                {
                    "id": 3,
                    "code": 3,
                    "title": "РЫБЫ И РАКООБРАЗНЫЕ,МОЛЛЮСКИ И ДРУГИЕ ВОДНЫЕ БЕСПОЗВОНОЧНЫЕ"
                },
                {
                    "id": 4,
                    "code": 4,
                    "title": "МОЛОЧНАЯ ПРОДУКЦИЯ;ЯЙЦА ПТИЦ;МЕД НАТУРАЛЬНЫЙ;ПИЩЕВЫЕ ПРОДУКТЫ ЖИВОТНОГО ПРОИСХОЖ"
                },
                {
                    "id": 5,
                    "code": 5,
                    "title": "ПРОДУКТЫ ЖИВОТНОГО ПРОИСХОЖДЕНИЯ,В ДРУГОМ МЕСТЕ НЕ ПОИМЕНОВАННЫЕ ИЛИ НЕ ВКЛЮЧЕН"
                }
            ]
        );
    });

    postRouter.get('/foreign_trade_by_country_export', function (req, res) {
        res.send(
            [
                {
                    "id": 370,
                    "code": 1,
                    "year": 2011,
                    "weight": 3286,
                    "usd_sum": 1887.5,
                    "som_sum": 87085
                },
                {
                    "id": 371,
                    "code": 2,
                    "year": 2011,
                    "weight": 22.5,
                    "usd_sum": 24.6,
                    "som_sum": 1141
                },
                {
                    "id": 372,
                    "code": 3,
                    "year": 2011,
                    "weight": 1418.3,
                    "usd_sum": 7693,
                    "som_sum": 353896
                },
                {
                    "id": 373,
                    "code": 4,
                    "year": 2011,
                    "weight": 407328,
                    "usd_sum": 284953,
                    "som_sum": 13057900
                }
            ]
        );
    });

    app.use('/', postRouter);
};
