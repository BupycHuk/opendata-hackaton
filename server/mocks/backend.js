module.exports = function (app) {
    var express = require('express');
    var eaesRouter = express.Router();
    var data = [
        {
            "id": 1,
            "code": 1,
            "title": "АЗЕРБАЙДЖАН"
        },
        {
            "id": 2,
            "code": 2,
            "title": "АРМЕНИЯ"
        },
        {
            "id": 3,
            "code": 3,
            "title": "БЕЛАРУСЬ"
        },
        {
            "id": 4,
            "code": 4,
            "title": "КАЗАХСТАН"
        }
    ];

    eaesRouter.get('/eaes-countries/:type/:id/', function (req, res) {
        res.send(
            [
                {
                    id: 1,
                    label: "Мясо",
                    values: [11585.3, 13153, 10431]
                },
                {
                    id: 2,
                    label: "Молоко",
                    values: [14585.3, 12153, 11431]
                }
            ]
        );
    });

    eaesRouter.get('/eaes-products/:type/:id/', function (req, res) {
        res.send(
            [
                {
                    id: 1,
                    label: "АЗЕРБАЙДЖАН",
                    values: [11585.3, 13153, 10431]
                },
                {
                    id: 2,
                    label: "АРМЕНИЯ",
                    values: [14585.3, 12153, 11431]
                }
            ]
        );
    });

    eaesRouter.get('/eaes-sankey/:type/', function (req, res) {
        res.send(
            [
                {
                    product: "Meat and edible meat offal nes",
                    country: "АРМЕНИЯ",
                    usd: 10.4,
                    year: 2001
                },
                {
                    product: "Meat and edible meat offal nes",
                    country: "АЗЕРБАЙДЖАН",
                    usd: 53.4,
                    year: 2002
                },
                {
                    product: "Meat and edible meat offal nes",
                    country: "АРМЕНИЯ",
                    usd: 257.4,
                    year: 2003
                },
                {
                    product: "Meat and edible meat offal nes",
                    country: "АРМЕНИЯ",
                    usd: 257.4,
                    year: 2004
                }
            ]
        );
    });

    eaesRouter.get('/eaes-countries', function(req, res) {
        res.send(
            data
        );
    });

    eaesRouter.get('/eaes-products', function(req, res) {
        res.send(
            [
                {
                    id: 1,
                    code: "0208",
                    title: "Meat and edible meat offal nes"
                },
                {
                    id: 2,
                    code: "0202",
                    title: "Meat of bovine animals, frozen"
                },
                {
                    id: 3,
                    code: "0201",
                    title: "Meat of bovine animals, fresh or chilled"
                },
                {
                    id: 4,
                    code: "0203",
                    title: "Meat of swine, fresh, chilled or frozen"
                },
                {
                    id: 5,
                    code: "0204",
                    title: "Meat of sheep or goats - fresh, chilled or frozen"
                }
                ]
        );
    });

    eaesRouter.get('/foreign_trade_country', function (req, res) {
        res.send(
            data
        );
    });

    app.use('/', eaesRouter);
};
